const commonUtils = {};

commonUtils.capitalizeFirstLetter = (word) => { 
    return word.charAt(0).toUpperCase() + word.slice(1);
}

commonUtils.randomElement = (array) => {
    return array[Math.floor(Math.random() * array.length)]
}

module.exports = commonUtils;