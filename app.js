const express = require('express')
const app = express()
const path = require("path");

const askLuisController = require("./controllers/askLuisController");

app.use(express.static('public'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/api/askLuis', askLuisController.ask);

app.listen(3000, function () {
    console.log('NDI 2018 is UP and RUNNING on port 3000!')
});