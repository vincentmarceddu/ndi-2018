$(document).ready(function () {


    $('img[usemap]').rwdImageMaps();

    $(".human-body-part").on({
        mouseenter: function () {
            //stuff to do on mouse enter
            $("#human-body-part").text($(this).data("body")); 
            $("#human-body-stat").text($(this).data("stat")); 
        },
        mouseleave: function () {
            //stuff to do on mouse leave
        }
    });
    //CHART JS GLOBAL CONFIGURATION
    Chart.defaults.scale.gridLines.display = false;


    Chart.pluginService.register({
        beforeDraw: function (chart) {
            if (chart.config.options.elements.center) {
                //Get ctx from string
                var ctx = chart.chart.ctx;

                //Get options from the center object in options
                var centerConfig = chart.config.options.elements.center;
                var fontStyle = centerConfig.fontStyle || 'Arial';
                var txt = centerConfig.text;
                var color = centerConfig.color || '#000';
                var sidePadding = centerConfig.sidePadding || 20;
                var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                //Start with a base font of 30px
                ctx.font = "30px " + fontStyle;

                //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                var stringWidth = ctx.measureText(txt).width;
                var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                // Find out how much the font can grow in width.
                var widthRatio = elementWidth / stringWidth;
                var newFontSize = Math.floor(30 * widthRatio);
                var elementHeight = (chart.innerRadius * 2);

                // Pick a new font size so it will not be larger than the height of label.
                var fontSizeToUse = Math.min(newFontSize, elementHeight);

                //Set font settings to draw it correctly.
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                ctx.font = fontSizeToUse + "px " + fontStyle;
                ctx.fillStyle = color;

                //Draw text in center
                ctx.fillText(txt, centerX, centerY);
            }
        }
    });

    //WEATHER
    loadWeather('Strasbourg',''); //@params location, woeid

    //HEALTH STATUS CHARTS
    let healthStateHungerCtx = document.getElementById("health-state-hunger-chart").getContext('2d');
    
    var hungerNumber = 89;
    var healthStateHungerChart = new Chart(healthStateHungerCtx, {
        type: 'doughnut',
        data: {
            labels: ["", "Faim"],
            datasets: [{
                label: "% of hunger",
                data: [100 - hungerNumber, hungerNumber],
                backgroundColor: [
                    'rgba(0, 0, 0, 0)',
                    'rgba(255, 99, 132, 0.2)'
                ],
                borderColor: [
                    'rgba(0, 0, 0, 0)',
                    'rgba(255,99,132,1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            elements: {
                center: {
                    text: hungerNumber + '%',
                    color: "rgba(255, 99, 132)", // Default is #000000
                    fontStyle: 'Arial', // Default is Arial
                    sidePadding: 20 // Default is 20 (as a percentage)
                }
            }
        }
    });


    
    let healthStateThirstCtx = document.getElementById("health-state-thirst-chart").getContext('2d');
    
    var thirstNumber = 68;
    var healthStateThirstChart = new Chart(healthStateThirstCtx, {
        type: 'doughnut',
        data: {
            labels: ["", 'Soif'],
            datasets: [{
                label: "% of thirst",
                data: [100 - thirstNumber, thirstNumber],
                backgroundColor: [
                    'rgba(0, 0, 0, 0)',
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(0, 0, 0, 0)',
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            elements: {
                center: {
                    text: thirstNumber + '%',
                    color: "rgba(54, 162, 235, 1)", // Default is #000000
                    fontStyle: 'Arial', // Default is Arial
                    sidePadding: 20 // Default is 20 (as a percentage)
                }
            }
        }
    });

    let healthStateSleepCtx = document.getElementById("health-state-sleep-chart").getContext('2d');
    
    var sleepNumber = 70;

    var healthStateSleepChart = new Chart(healthStateSleepCtx, {
        type: 'doughnut',
        data: {
            labels: ["", 'Sommeil'],
            datasets: [{
                label: "% of sleep",
                data: [100 - sleepNumber, sleepNumber],
                backgroundColor: [
                    'rgba(0, 0, 0, 0)',
                    'rgba(107, 73, 132, 0.2)'
                ],
                borderColor: [
                    'rgba(0, 0, 0, 0)',
                    'rgba(107, 73, 132, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            elements: {
                center: {
                    text: sleepNumber + '%',
                    color: "rgba(107, 73, 132, 1)", // Default is #000000
                    fontStyle: 'Arial', // Default is Arial
                    sidePadding: 20 // Default is 20 (as a percentage)
                }
            }
        }
    });

    var thirst = document.getElementById('health-state-hunger-chart');

    function loadWeather(location, woeid,) {
        $.simpleWeather({
            location: location,
            woeid: woeid,
            unit: 'c',
            success: function(weather) {
                html = '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';
                html += '<ul><li>'+weather.city+', '+weather.region+'</li>';
                html += '<li class="currently">'+weather.currently+'</li>';
                html += '<li>Humidité : '+weather.humidity+'%</li></ul>';  
            
                $("#weather").html(html);
            },
            error: function(error) {
                $("#weather").html('<p>'+error+'</p>');
            }
        });
    }



    // let healthStateHungerChart = new Chart(healthStateHungerCtx, {
    //     type: 'bar',
    //     data: {
    //         labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
    //         datasets: [{
    //             label: '# of Votes',
    //             data: [12, 19, 3, 5, 2, 3],
    //             backgroundColor: [
    //                 'rgba(255, 99, 132, 0.2)',
    //                 'rgba(54, 162, 235, 0.2)',
    //                 'rgba(255, 206, 86, 0.2)',
    //                 'rgba(75, 192, 192, 0.2)',
    //                 'rgba(153, 102, 255, 0.2)',
    //                 'rgba(255, 159, 64, 0.2)'
    //             ],
    //             borderColor: [
    //                 'rgba(255,99,132,1)',
    //                 'rgba(54, 162, 235, 1)',
    //                 'rgba(255, 206, 86, 1)',
    //                 'rgba(75, 192, 192, 1)',
    //                 'rgba(153, 102, 255, 1)',
    //                 'rgba(255, 159, 64, 1)'
    //             ],
    //             borderWidth: 1
    //         }]
    //     },
    //     options: {
    //         scales: {
    //             yAxes: [{
    //                 ticks: {
    //                     beginAtZero:true
    //                 }
    //             }]
    //         }
    //     }
    // });


    let gui = new dat.GUI();
    gui.close(); 
    let darkThemFolder = gui.addFolder("Texte"); 
    let parameters = {
        darkTheme: false, 
        color: "#00ff00"
    }

    let darkTheme = gui.add(parameters, "darkTheme"); 
    let chartColor = gui.addColor({color : "#ffff00"}, 'color');
    
    darkTheme.onChange(function(value){
        if(value){
            $("body").css({"background":"url('../img/pattern.png')"});
            $("hr").css({"border-color":"white"});
            $("h2").css({"color":"white"});
            $("i").css({"color":"white"});
            $(".human-body-info>*").css({"color":"white"});
            $(".human-body-info").css({"border-color":"white"});
            $("#weather li").css({"border-color":"white"});
            $("#weather li").css({"color":"white"});
        } else {
            $("body").css({"background":"url('../img/pattern-light.png')"});
            $("hr").css({"border-color":"black"});
            $("h2").css({"color":"black"});
            $("i").css({"color":"black"});
            $(".human-body-info>*").css({"color":"black"});
            $(".human-body-info").css({"border-color":"black"});
            $("#weather li").css({"border-color":"black"});
            $("#weather li").css({"color":"black"});
        }
    })

    chartColor.onChange(function(value){
        let colors = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(value); 	

        $("h2").css({
            "color" : value
        });
    }); 


    let hungerColor = gui.addFolder("Faim");
    hungerColor.addColor({color : "#ffff00"}, 'color').onChange(function(value){

        let colors = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(value); 	

        let r = parseInt(colors[1], 16);     
        let g = parseInt(colors[2], 16);     
        let b = parseInt(colors[3], 16); 	

        healthStateHungerChart.data.datasets[0].backgroundColor= ['rgba(0, 0, 0, 0)', `rgba(${r},${g},${b},0.6)`];
        healthStateHungerChart.data.datasets[0].borderColor= ['rgba(0, 0, 0, 0)', `rgba(${r},${g},${b},1)`];
        healthStateHungerChart.options.elements.center.color = `rgba(${r},${g},${b},1)`; 
        healthStateHungerChart.update(); 
    });

    let thirstColor = gui.addFolder("Soif");
    thirstColor.addColor({color : "#ffff00"}, 'color').onChange(function(value){

        let colors = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(value); 	

        let r = parseInt(colors[1], 16);     
        let g = parseInt(colors[2], 16);     
        let b = parseInt(colors[3], 16); 	

        healthStateThirstChart.data.datasets[0].backgroundColor= ['rgba(0, 0, 0, 0)', `rgba(${r},${g},${b},0.6)`];
        healthStateThirstChart.data.datasets[0].borderColor= ['rgba(0, 0, 0, 0)', `rgba(${r},${g},${b},1)`];
        healthStateThirstChart.options.elements.center.color = `rgba(${r},${g},${b},1)`; 
        healthStateThirstChart.update(); 
    });

    let sleepColor = gui.addFolder("Sommeil");
    sleepColor.addColor({color : "#ffff00"}, 'color').onChange(function(value){

        let colors = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(value); 	

        let r = parseInt(colors[1], 16);     
        let g = parseInt(colors[2], 16);     
        let b = parseInt(colors[3], 16); 	

        healthStateSleepChart.data.datasets[0].backgroundColor= ['rgba(0, 0, 0, 0)', `rgba(${r},${g},${b},0.6)`];
        healthStateSleepChart.data.datasets[0].borderColor= ['rgba(0, 0, 0, 0)', `rgba(${r},${g},${b},1)`];
        healthStateSleepChart.options.elements.center.color = `rgba(${r},${g},${b},1)`; 
        healthStateSleepChart.update(); 
    }); 

    

 


    let iconeColor = gui.addFolder("Icône");
    iconeColor.addColor({color : "#ffff00"}, 'color').onChange(function(value){
        let iconeHomeCtx = document.getElementById("maison");
       let iconeWeatherCtx = document.getElementsByClassName("icon-26");

        iconeHomeCtx.style.color = value;
        iconeWeatherCtx[0].style.color = value; 

    }); 

});