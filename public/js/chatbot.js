let isOpened = false;

let chatbot = $("#chatbot-container");
let chatbotContent = $("#chatbot-container>.chatbot-content");
let chatbotInputs = $("#chatbot-container>.chatbot-inputs");
let chatbotTitle = $("#chatbot-container>.chatbot-title");
let chatbotButtonMobile = $(".chatbot-mobile-button");

let isMobile;

let heightToRemove = chatbotContent.outerHeight() + chatbotInputs.outerHeight();

if (window.matchMedia("(min-width: 740px)").matches) {

    isMobile = false;
} else {
    isMobile = true;
}

chatbot.css({ "transform": `translateY(${heightToRemove}px)` });

chatbotTitle.on("click", function () {
    heightToRemove = chatbotContent.outerHeight() + chatbotInputs.outerHeight();
    if (!isOpened) {
        let popOut = anime({
            targets: '#chatbot-container',
            translateY: 0,
            easing: [.91, -0.54, .29, 1.56],
            duration: 500
        });
    }
    else {
        let popOut = anime({
            targets: '#chatbot-container',
            translateY: heightToRemove,
            easing: [.91, -0.54, .29, 1.56],
            duration: 500
        });
    }

    isOpened = !isOpened;
});

$("#chatbot-close-button").on("click", function () {
    console.log(isMobile);
    if (isMobile) {
        chatbot.removeClass("chatbot-opened-mobile");
    }
    else {
        let popOut = anime({
            targets: '#chatbot-container',
            translateY: 0,
            easing: [.91, -0.54, .29, 1.56],
            duration: 500
        });
    }
});

$(window).resize(function () {
    if (window.matchMedia("(min-width: 740px)").matches) {
        chatbot.removeClass("chatbot-opened-mobile");

        let heightToRemove = chatbotContent.outerHeight() + chatbotInputs.outerHeight();
        chatbot.css({ "transform": `translateY(${heightToRemove}px)` });
        isOpened = false;

    } else {
        isMobile = true;
    }
});

chatbotButtonMobile.on("click", function () {
    chatbot.addClass("chatbot-opened-mobile");
});

$("#chatbot-container #utterance").keypress(function (e) {
    if (e.which == 13) {
        setMessage($("#chatbot-container #utterance").val());
        parseUserMessage();
        $("#chatbot-container #utterance").val("");
    }
});

$("#chatbot-container #submit").on('click', () => {
    setMessage($("#chatbot-container #utterance").val());
    parseUserMessage();
});

function parseUserMessage() {
    let userMessage = $("#chatbot-container #utterance").val();
    if (userMessage === "Présente-moi ton ami") {
        let pinataContainer = $(".pinata-container");
        if (pinataContainer.is(':visible')) {
            setMessage({"label":"Donc t'es aveugle en plus ? Il est déjà sur la page.", "mood":"eustache-colere"}, "bot");
        } else {
            setMessage({"label":"Attention, il est un peu timide. Trouve-le 7 fois de suite et il te donnera une récompense.", "mood":"eustache-espiegle"}, "bot");
            pinataContainer.show();
        }
    } else {
        askLuis(userMessage);
    }
}

function askLuis(userMessage) {
    $.ajax({
        url: "/api/askLuis",
        data: {
            "utterance": userMessage
        },
        cache: false,
        type: "GET",
        success: function (response) {
            if (Array.isArray(response)) {
                let currentResponseIndex = 0;
                let timer = setInterval(function () {
                    setMessage(response[currentResponseIndex], "bot");
                    currentResponseIndex++;
                    if (currentResponseIndex === response.length) {
                        clearInterval(timer);
                    }
                }, 2000);
            } else {
                setMessage(response, "bot");
            }
        },
        error: function (xhr) {
            console.log("luis error");
        }
    });
}

function setMessage(message, user) {
    let waitDuration = Math.floor(Math.random() * Math.floor(1500)) + 500;

    if (user == "bot") {

        getWaitMessage(waitDuration);

        setTimeout(function () {
            chatbotContent.append(`
            <div class="field is-grouped msg">
                <p class="control msg-user">
                    <img src="img/${message.mood}.png" alt="">
                </p>
                <p class="control is-expanded msg-content">
                    ${message.label}
                </p>
            </div>`);
            let showMessage = anime({
                targets: ".msg",
                translateY: -10,
                opacity: 1,
                autoplay: true
        
            });
        
            chatbotContent.scrollTop(100000000000000000);
        }, waitDuration);
    } else {
        chatbotContent.append(`
            <div class="field is-grouped msg">
                <p class="control is-expanded msg-content">
                    ${message}
                </p>
                <p class="control msg-user">
                    <img src="img/user.png" alt="">
                </p>
            </div>`);
            let showMessage = anime({
                targets: ".msg",
                translateY: -10,
                opacity: 1,
                autoplay: true
        
            });
        
            chatbotContent.scrollTop(100000000000000000);
    }

}

function getWaitMessage(duration) {

    chatbotContent.append(`
    <div id="pending-message" class="field is-grouped msg loader-msg">
        <p class="control msg-user">
            <img src="img/eustache-normal.png" alt="">
        </p>
        <div class="msg-content ">
            <div class="loading-dots">
                <div class="dot"></div>
                <div class="dot"></div>
                <div class="dot"></div>
            </div>  
        </div>
    </div>`);

    chatbotContent.scrollTop(100000000000000000);

    setTimeout(function () {
        $("#pending-message").remove();
    }, duration);
}