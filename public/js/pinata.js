var pinata = $("#pinata");
var actualPosition = 3;
let canClick = true;

let amountClicked = 0;

$(".pinata-reward").css({"display":"none"});


pinata.on("click", function () {

    if(canClick){
        shakePinata(actualPosition);
    }
});

pinata.on("dblclick", function () {
    amountClicked++;
    if (amountClicked == 7) {
        canClick = false;
        winAnimation();
    } else {
        if(canClick){
            hidePinata(actualPosition);
            setTimeout(movePinata, 2000);
        }
    }
});

function movePinata() {
    var position = getNewPosition();
    pinata.css("left", "auto").css("right", "auto").css("top", "auto").css("bottom", "auto");
    pinata.css("transform", "");

    var windowWidth = $(window).width() - 100;
    var windowHeight = $(window).height() - 100;

    var verticalPosRandom = getRandomInt(windowHeight);
    var horizontalPosRandom = getRandomInt(windowWidth);

    switch (position) {
        case 0:
            //LEFT
            pinata.css({ left: -400, position: 'fixed' });
            pinata.css('top', verticalPosRandom);
            pinata.rotate(90);
            break;

        case 1:
            //RIGHT
            pinata.css({ right: -400, position: 'fixed' });
            pinata.css('top', verticalPosRandom);
            pinata.rotate(-90);
            break;

        case 2:
            //TOP
            pinata.css({ top: -400, position: 'fixed' });
            pinata.css('right', horizontalPosRandom);
            pinata.rotate(-180);
            break;

        case 3:
            //BOTTOM
            pinata.css({ bottom: -400, position: 'fixed' });
            pinata.css('right', horizontalPosRandom);
            pinata.rotate(0);
            break;

        default:
            console.log("ERR: Position de la piñata inconnue");
            break;
    }
    showPinata(position);
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function getNewPosition() {
    var newPosition = getRandomInt(4);
    if (actualPosition == newPosition) {
        console.log("Position identiques");
        return getNewPosition();
    }
    else {
        actualPosition = newPosition;
        console.log("Nouvelle position");
        return newPosition;
    }
}

jQuery.fn.rotate = function (degrees) {
    $(this).css({
        '-webkit-transform': 'rotate(' + degrees + 'deg)',
        '-moz-transform': 'rotate(' + degrees + 'deg)',
        '-ms-transform': 'rotate(' + degrees + 'deg)',
        'transform': 'rotate(' + degrees + 'deg)'
    });
    return $(this);
};

function hidePinata(position) {
    switch (position) {
        case 0:
            //LEFT
            var a = anime({
                targets: '#pinata',
                left: -400,
                rotate: [90, 90],
                easing: [.91, -0.54, .29, 1.56]
            });
            break;
        case 1:
            //RIGHT
            var b = anime({
                targets: '#pinata',
                right: -400,
                rotate: [-90, -90],
                easing: [.91, -0.54, .29, 1.56]
            });
            break;
        case 2:
            //TOP
            var v = anime({
                targets: '#pinata',
                top: -400,
                rotate: [180, 180],
                easing: [.91, -0.54, .29, 1.56]
            });
            break;
        case 3:
            //BOTTOM
            var c = anime({
                targets: '#pinata',
                bottom: -400,
                easing: [.91, -0.54, .29, 1.56]
            });
            break;
        default:
            console.log("ERR: Position de la piñata inconnue")
            break;
    }
}

function showPinata(position) {
    switch (position) {
        case 0:
            //LEFT
            var a = anime({
                targets: '#pinata',
                left: -120,
                rotate: [90, 90],
                easing: [.91, -0.54, .29, 1.56]
            });
            break;
        case 1:
            //RIGHT
            var b = anime({
                targets: '#pinata',
                right: -120,
                rotate: [-90, -90],
                easing: [.91, -0.54, .29, 1.56]
            });
            break;
        case 2:
            //TOP
            var v = anime({
                targets: '#pinata',
                top: -150,
                rotate: [180, 180],
                easing: [.91, -0.54, .29, 1.56]
            });
            break;
        case 3:
            //BOTTOM
            var c = anime({
                targets: '#pinata',
                bottom: -150,
                easing: [.91, -0.54, .29, 1.56]
            });
            break;
        default:
            console.log("ERR: Position de la piñata inconnue")
            break;
    }
}

function shakePinata(position) {
    const xMax = 8;
    switch (position) {
        case 0:
            //LEFT
            var a = anime({
                targets: '#pinata',
                rotate: [90, 90],
                easing: 'easeInOutSine',
                duration: 550,
                translateX: [
                    {
                        value: xMax * -1,
                    },
                    {
                        value: xMax,
                    },
                    {
                        value: xMax / -2,
                    },
                    {
                        value: xMax / 2,
                    },
                    {
                        value: 0
                    }
                ],
            });
            break;
        case 1:
            //RIGHT
            var a = anime({
                targets: '#pinata',
                rotate: [-90, -90],
                easing: 'easeInOutSine',
                duration: 550,
                translateX: [
                    {
                        value: xMax * -1,
                    },
                    {
                        value: xMax,
                    },
                    {
                        value: xMax / -2,
                    },
                    {
                        value: xMax / 2,
                    },
                    {
                        value: 0
                    }
                ],
            });
            break;
        case 2:
            //TOP
            var a = anime({
                targets: '#pinata',
                rotate: [180, 180],
                easing: 'easeInOutSine',
                duration: 550,
                translateX: [
                    {
                        value: xMax * -1,
                    },
                    {
                        value: xMax,
                    },
                    {
                        value: xMax / -2,
                    },
                    {
                        value: xMax / 2,
                    },
                    {
                        value: 0
                    }
                ],
            });
            break;
        case 3:
            //BOTTOM
            var a = anime({
                targets: '#pinata',
                easing: 'easeInOutSine',
                duration: 550,
                translateX: [
                    {
                        value: xMax * -1,
                    },
                    {
                        value: xMax,
                    },
                    {
                        value: xMax / -2,
                    },
                    {
                        value: xMax / 2,
                    },
                    {
                        value: 0
                    }
                ],
            });
            break;
        default:
            console.log("ERR: Position de la piñata inconnue")
            break;
    }
}

movePinata();

function winAnimation() {

    pinata.css("left", "auto").css("right", "auto").css("top", "auto").css("bottom", "auto");
    pinata.css("transform", "");

    pinata.css({ top: "50%", position: 'fixed' });
    pinata.css('right', "50%");
    pinata.css("transform", "translate(50%, -50%) scale(0.1)");
    let testo = anime({
        targets: '#pinata',
        easing: 'linear',
        duration: 1000,
        translateX: "50%",
        translateY: "-50%",
        scale: 3,
        rotate: "-1080deg",
        complete: function (anim) {
            $('#pinata>img').css({ opacity: "0" });
            $('#pinata').data("fun", "4");

            let buttons = Array.from(document.querySelectorAll('[data-fun]'));
            buttons.forEach(button => {
                    window.confetti(button, confettis_conf[button.getAttribute('data-fun')]);
            });

            $(".pinata-reward").css({"display":"inline"});

            
            setTimeout(function () {
                $("#pinata").css({display:"none"});
            }, 1500);
        }
    });
}

$(".pinata-reward").on("click", function(){
    $(".pinata-reward").css({"display":"none"});
});
