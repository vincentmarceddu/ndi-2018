![](https://i.ibb.co/tMmHgbq/logo-coddity.png)

# Table des matières

 1. Le Ballet des Dauphins Chanteurs
 2. Procédure d'installation en local
 3. Choix techniques
 4. Comportement Piñata


# 1. Le Ballet des Dauphins Chanteurs

Nous sommes 5 étudiants à l’école d’ingénieur du CNAM. Le Ballet des Dauphins Chanteurs, composé de cétacés rieurs, décident de relever le défi de Coddity, qui vient chatouiller leur sens de la créativité et de l'humour concernant un domaine qui nous est cher. 


# 2. Procédure d'installation en local

Pour installer notre superbe Piñata en local, c'est très simple ! 
Il suffit de cloner le repository Git en utilisant la commande suivante :
`git clone https://gitlab.com/vincentmarceddu/ndi-2018.git`
Puis d'installer Node.js sur votre machine. https://nodejs.org/en/download/
Ensuite, vous pourrez lancer le serveur node js avec la commande "node app.js", puis vous allez pouvoir accéder à la page grâce au lien "127.0.0.1:3000" dans votre navigateur favoris.


# 3. Choix techniques

En ce qui concerne les choix techniques, nous avons choisi **anime.js**.
Cette bibliothèque facilite l'animation avec javascript et permet de créer des animations fluides rapidement.

Pour les confettis, nous avons spécialement créé une petite bibliothèque qui génère des balises "li" vides et une forme associée. Un effet de propulsion et de gravité sont affectés aux formes pour simuler l'explosion de confettis.


# 4. Comportement Piñata

Pour faire apparaître la Piñata, il faut demander au chatbot Eustache ; "Présente-moi ton ami". 
La Piñata apparaîtra alors quelque part au bord de l'écran, et il faudra double-cliquer dessus 7 fois d'affilée pour activer un message de victoire. Si vous ne faites qu'un clique simple sur la Piñata, cela n'activera que les confettis et un effet de tremblement.
Notre Piñata est **responsive**, elle fonctionne et s'adapte sur mobile.

> **Note :** L'apparition de la Piñata grâce à notre chatbot **Eustache** est un Cross-Over avec le défi proposé par CGI.