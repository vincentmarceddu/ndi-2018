const commonUtils = require('../../utils/commonUtils');
const LINQ = require("node-linq").LINQ;

const informationRequestController = {};
informationRequestController.jsonContent = require('./sentenceTemplates/presentItself.json');

informationRequestController.getAnswer = (response) => {
    return new Promise((resolve, reject) => resolve(commonUtils.randomElement(informationRequestController.jsonContent)));
};

module.exports = informationRequestController;

