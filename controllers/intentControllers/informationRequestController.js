const commonUtils = require('../../utils/commonUtils');
const request = require('request');
const querystring = require('querystring');
const LINQ = require("node-linq").LINQ;

const informationRequestController = {};
informationRequestController.jsonContent = require('./sentenceTemplates/informationRequest.json');

informationRequestController.getAnswer = (response) => {
    let answer;

    //Information Request sur les concurrents
    let concurrentEntities = new LINQ(response.entities).Where((entity) => entity.type === "CGI_Concurrent");
    if (concurrentEntities.items.length > 0) {
        let entityString = new LINQ(concurrentEntities.items).Select(concurrentEntity => commonUtils.capitalizeFirstLetter(concurrentEntity.entity)).items.join(', ');
        answer = { "label": `Hum, je n'ai aucune information sur ${entityString}, vouliez-vous dire CGI ?`, "mood": "eustache-glados" };
    } else {

        let answerType = commonUtils.randomElement(["openAnswer", "closeAnswer"]);

        if (answerType === "openAnswer") {
            let keyWord = new LINQ(response.entities).Where((entity) => entity.type === "builtin.keyPhrase").Select((entity) => entity.entity).items[0];
            answer = informationRequestController.getFromWikipedia(keyWord);
        } else {
            answer = new Promise((resolve, reject) => resolve({"label":commonUtils.randomElement(informationRequestController.jsonContent.closeAnswer), "mood":"eustache-colere"}));
        }
    }
    return new Promise((resolve, reject) => resolve(answer));
};

let endpoint =
    "https://fr.wikipedia.org/w/api.php";

informationRequestController.getFromWikipedia = (keyPhrase) => {

    var queryParams = {
        "action": "opensearch",
        "search": keyPhrase,
        "limit": 1,
        "namespace": 0,
        "format": "json"
    }

    var wikipediaRequest =
        endpoint + '?' + querystring.stringify(queryParams);

    return new Promise(function (resolve, reject) {
        request(wikipediaRequest,
            function (err, response, body) {
                if (!err && response.statusCode == 200) {
                    resolve([
                        {
                            "label": "J'suis de bonne humeur, voici les informations de wikipédia : ",
                            "mood": "eustache-normal"
                        },
                        {
                            "label": JSON.parse(body)[2][0],
                            "mood": "eustache-normal"
                        }]);
                } else {
                    reject(err);
                }
            });
    });
}


module.exports = informationRequestController;

