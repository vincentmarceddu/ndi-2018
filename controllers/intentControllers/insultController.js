const commonUtils = require('../../utils/commonUtils');
const LINQ = require("node-linq").LINQ;

const insultController = {};

insultController.insultCount = 0;

insultController.getAnswer = (response) => {
    insultController.insultCount++;

    let insultEntities = new LINQ(response.entities).Where((entity) => entity.type === "Insult");
    let entityString;
    if (insultEntities.items.length > 0) {
        entityString = new LINQ(insultEntities.items).Select(insultEntity => commonUtils.capitalizeFirstLetter(insultEntity.entity)).items.join(', ');
    }
    


    let answer =
        [
            {
                "label": "Je suis immunisé contre vos sarcasme.",
                "mood": "eustache-colere"
            },
            {
                "label": "Et puis, c'est celui qui dit qui est.",
                "mood": "eustache-normal"
            }
        ]

        if(entityString != undefined){
            answer.unshift(
            {
                "label": `Moi ? ${entityString} ?`,
                "mood": "eustache-colere"
            });
        }
        
    return new Promise((resolve, reject) => resolve(answer));
};

module.exports = insultController;

