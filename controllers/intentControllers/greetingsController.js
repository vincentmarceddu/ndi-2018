const commonUtils = require('../../utils/commonUtils');

const greetingsController = {};
greetingsController.jsonContent = require('./sentenceTemplates/greetings.json');

greetingsController.getAnswer = (response) => {
    return new Promise((resolve, reject) => resolve(commonUtils.randomElement(greetingsController.jsonContent)));
};

module.exports = greetingsController;

