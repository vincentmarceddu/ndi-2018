const commonUtils = require('../../utils/commonUtils');
const LINQ = require("node-linq").LINQ;

const complimentController = {};

complimentController.jsonContent = require('./sentenceTemplates/compliment.json');

complimentController.getAnswer = (response) => {
    return new Promise((resolve, reject) => resolve(commonUtils.randomElement(complimentController.jsonContent)));
};

module.exports = complimentController;

