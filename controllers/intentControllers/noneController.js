const noneController = {};

noneController.getAnswer = (response) => {
    return new Promise((resolve, reject) => resolve({"label":"J'ai rien compris, apprends à formuler tes idées.", "mood":"eustache-glados"}));
};

module.exports = noneController;

