const commonUtils = require('../../utils/commonUtils');

const tellMeAJokeController = {};
tellMeAJokeController.jsonContent = require('./sentenceTemplates/tellMeAJoke.json');

tellMeAJokeController.getAnswer = (response) => {
    return new Promise((resolve, reject) => resolve(commonUtils.randomElement(tellMeAJokeController.jsonContent)));
};

module.exports = tellMeAJokeController;

